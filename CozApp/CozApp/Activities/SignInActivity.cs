﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Gms.Common.Apis;
using Android.Gms.Plus;
using Android.Gms.Common;
using Android.Support.Design.Widget;

namespace CozApp.Activities
{
	[Activity (Label = "SignInActivity")]			
	public class SignInActivity : AppCompatActivity, GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
	{
		ImageButton _signInButton;

		GoogleApiClient _client;

		bool _mIsResolving = false;
		bool _mShouldResolve = false;
		const int RC_SIGN_IN = 9001;

		ProgressDialog _signInInProgress;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.SignIn);

			Initialise ();
			 
			CreateOnClickEvents ();
			
		}

		/// <summary>
		/// Initialise sign in button &
		/// Google play api 
		/// </summary>
		void Initialise(){
			_signInButton = FindViewById<ImageButton> (Resource.Id.signInButton);

			_client = new GoogleApiClient.Builder (this) 
				.AddConnectionCallbacks (this) 
 				.AddOnConnectionFailedListener (this) 
 				.AddApi (PlusClass.API)
				.AddScope (PlusClass.ScopePlusProfile)
 				.Build (); 
			 
		}

		/// <summary>
		/// Creates the on click events.
		/// </summary>
		void CreateOnClickEvents(){
			_signInButton.Click += (object sender, EventArgs e) => {		
				_mShouldResolve = true;
				_signInInProgress = ProgressDialog.Show(this, "Sign in", "");
				_client.Connect ();
			};
		
		}

		public void OnConnected (Bundle connectionHint)
		{				
			
			BaseActivity._isLoggedIn = true;
			Intent intent = new Intent (this, typeof(MainActivity));
			StartActivity (intent);
			Finish ();
		}

		public void OnConnectionSuspended (int cause)
		{
			throw new NotImplementedException ();
		}

		public void OnConnectionFailed (ConnectionResult result)
		{

			if (!_mIsResolving && _mShouldResolve) {
				if (result.HasResolution) {
					try {
						result.StartResolutionForResult (this, RC_SIGN_IN);
						_mIsResolving = true;
					} catch (IntentSender.SendIntentException e) {
						_mIsResolving = false;
						_client.Connect ();
					}
				} else {
					Snackbar
						.Make (_signInButton, "Unable to sign in", Snackbar.LengthShort)
						.Show (); // Don’t forget to show!					}
				}
			}
			
		}

		protected override void OnStart ()
		{
			base.OnStart ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
		}
	}
}

