﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Gms.Common.Apis;

namespace CozApp.Activities
{
	[Activity (Label = "BaseActivity")]			
	public class BaseActivity : AppCompatActivity
	{
		public static bool _isLoggedIn = false;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			CheckLoginState ();
		}

		private void CheckLoginState(){			
			if (!_isLoggedIn) {
				Intent intent = new Intent (this, typeof(SignInActivity));
				StartActivity (intent);
				Finish ();
			}
		}
	}
}

